pakcs (3.3.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Update versioned B-Ds and Ds for PAKCS components.
    + Bump Standards-Version: to 4.5.1. No changes needed.
  * debian/watch:
    + Update format version to 4.
  * debian/pakcs.install:
    + Update for renamed documentation files.
  * debian/pakcsinitrc:
    + Drop file. Not required anymore.
  * debian/rules:
    + Fix +DEB_DH_INSTALLCHANGELOGS_ARGS with correct (renamed) RELNOTES.md
      file name.
    + Drop some chmod operations for files that are gone in this release.
    * Adapt various file tweask to new upstream release.
    + Move more doc files to /usr/share/doc and symlink.
  * debian/pakcs.manpages:
    + Don't ship runcurry.1 man page. Binary has vanished in this upstream
      release.
  * debian/copyright:
    + Add auto-generated copyright.in template file.
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 04 Feb 2021 17:43:47 +0100

pakcs (2.2.0-2) unstable; urgency=medium

  * debian/control:
    + Bump Standards-Version: to 4.5.0. No changes needed.
    + Make sure we rebuild against curry-frontend (>= 1.0.4-5~).

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 29 Sep 2020 17:42:57 +0200

pakcs (2.2.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Bump versioned B-Ds and Ds for PAKCS 2.2.0.
    + Bump Standards-Version: to 4.4.1. No changes needed.
  * debian/move2docs_and_symlink.sh:
    + Fail on errors.
  * debian/rules:
    + Update move2docs command list (for tools/cpm/Implementation.md and
      tools/optimize/README.txt -> README.md).

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 14 Jan 2020 07:35:43 +0100

pakcs (2.1.1-2) unstable; urgency=medium

  * debian/pakcs.links:
    + Fix broken symlink from /usr/lib/pakcs/docs to the pkg's doc folder.
      (Closes: #924201).

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 17 Mar 2019 21:45:24 +0100

pakcs (2.1.1-1) unstable; urgency=medium

  * New upstream release.
    - Fix loading for external Prolog files. (Closes: #922024).
  * debian/pakcs.links:
    + Typo fix in link target path.
  * debian/rules:
    + Patch $(CURRYTOOLSDIR)/cpm/Makefile and fix installation path of the
      cpm template files.
    + Unset PAKCSBUILDDIR variables wherever used. Hide the build path of the
      pakcs DEB package.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 20 Feb 2019 15:35:00 +0100

pakcs (2.1.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright:
    + Update copyright attributions.
    + Sort listed files.
  * debian/control:
    + Bump Curry B-Ds and Ds to recent versions.
    + Bump Standards-Version: to 4.3.0. No changes needed.
  * debian/*:
    + Install documentation and example files into
      /usr/share/doc/pakcs and symlink them where needed.
  * debian/pakcs.lintian-overrides:
    + Drop file. Not required anymore.
  * debian/upstream/metadata:
    + Fix Repository-^Cowse: field.
  * debian/{rules,control}:
    + Use rdfind and symlinks to get rid of some duplicate documentation files
      (and symlink them instead).

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 02 Feb 2019 15:25:32 +0100

pakcs (2.0.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Rebase 2001_run-cleancurry-from-inside-debian-subfolder.patch.
  * debian/control:
    + Bump Standards-Version. to 4.2.1. No changes needed.
    + B-D and D on latest curry-frontend, curry-libs and curry-tools.
    + Drop from D (pakcs): libjs-prototype. Not required anymore by pakcs.
  * debian/upstream/metadata:
    + Add file. Comply with DEP-12.
  * debian/copyright:
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 23 Nov 2018 16:15:07 +0100

pakcs (2.0.1-2) unstable; urgency=medium

  * debian/control:
    + Update Vcs-*: fields. Package has been migrated to salsa.debian.org.
    + Bump Standards-Version: to 4.2.0. No changes needed.
  * debian/watch:
    + Update for tarball export feature in latest GitLab versions.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 20 Aug 2018 15:35:24 +0200

pakcs (2.0.1-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Bump Standards-Version: to 4.1.3. No changes needed.
    + Adapt versioned B-Ds and Ds for curry-* packages.
  * debian/{control,compat}:
    + Bump to DH version level 10 (level 11 not yet supported by CDBS).
  * debian/copyright:
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 09 Feb 2018 12:19:23 +0100

pakcs (2.0.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Bump versioned B-Ds and Ds (curry-*) to typeclasses supporting versions.
    + Enforce swi-prolog(-nox) (>= 7.4) as B-D and D. Important: PAKCS needs
      the same SWI Prolog version at runtime as it had at build time.
    + Set Priority: to optional.
    + Clean-up white-space at EOL.
  * debian/pakcs.{install,links}:
    + Adapt to this upstream release.
  * debian/rules:
    + Don't clean-up /usr/lib/pakcs/currytools/peval/.gitignore at post-build.
      Does not exist since PAKCS 1.15.x anymore.
    + Set permissions a-x on pakcsinitrc.sh (to make lintian happy).
    + Drop many LICENSE files shipped via the curry-tools tarball.
  * debian/copyright:
    + Update copyright attributions.
  * debian/patches:
    + Rebase/update 2001_run-cleancurry-from-inside-debian-subfolder.patch.
    + Drop 0001-Regression-test-for-CHR-library-removed-since-it-is-.patch.
      Applied upstream.
  * debian/man/runcurry.1: White-space cleanup at EOL.
  * debian/bin/cleancurry:
    + Drop file, use upstream's always-up-to-date version instead of shipping
      our own copy.
  * lintian: Drop various not-needed lintian overrides.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 12 Dec 2017 13:29:27 +0100

pakcs (1.14.4-2) unstable; urgency=medium

  * debian/patches:
    + Add 0001-Regression-test-for-CHR-library-removed-since-it-is-.patch
      from upstream. (Closes: #880797).

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 07 Nov 2017 14:07:07 +0100

pakcs (1.14.4-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    + Bump Standards-Version: to 4.1.1. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 03 Nov 2017 10:31:46 +0100

pakcs (1.14.3-1) unstable; urgency=medium

  * New upstream release. (Closes: #871194).
  * debian/watch:
    + Update to new tarball naming scheme in recent GitLab versions.
  * debian/control:
    + Bump Standards-Version: to 4.1.0. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 15 Sep 2017 14:42:27 +0200

pakcs (1.14.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Drop all patches recently cherry-picked from upstream. They are included
      in this upstream release.
  * debian/control:
    + Update versioned B-Ds for curry-libs-source (>= 1.0.3-1~) and
      curry-tools-source (>= 1.0.3+dfsg1-1~).
  * debian/pakcs.install:
    + The cpns/ and www/ code has been moved to curry-tools.
  * debian/copyright:
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 24 Feb 2017 12:44:15 +0100

pakcs (1.14.1-4) unstable; urgency=medium

  * debian/rules:
    + Don't run unit tests on 32bit hardware. The SWI-Prolog's stack size is to
      small for it on 32bit platforms.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 08 Feb 2017 17:18:04 +0100

pakcs (1.14.1-3) unstable; urgency=medium

  * debian/patches:
    + Add 0055-More-error-exit-codes-are-set.patch,
          0056-CASS-tool-updated-containing-new-analyses.patch,
          0057-Makefiloe-changed-for-package-installation-verbose-m.patch,
          0058-Tools-and-libs-updated.patch.
      Add more exit points with exit code.
      Support running tests in verbose mode.
    + Add 0059-lockfile-command-replaced-by-lockfile-create-remove.patch;
      use lockfile-progs instead of procmail's lockfile.
    + Add 0060-Bug-for-loading-libraries-with-SWI-Prolog-fixed-SWI-.patch,
      0061-Release-notes-and-build-version-number-adapted.patch; fix library
      loading issue with SWI Prolog.
    + Add 0062-tools-and-libs-updated.patch,
          0063-Check-for-existence-of-home-directory-extended.patch,
          0064-tools-updated.patch.
      Check existence of home directory + release notes updates.
    + Add 0065-tools-updated-clean-up-after-runtest-added-in-Makefi.patch,
          0066-noreadline-added-to-CHR-tests.patch,
          0067-pakcs-script-slightly-improved-w.r.t-home-dir-checki.patch.
      Prevent test suite from attempting to create non-existent home directory.
  * debian/control:
    + Add to B-D and R (pakcs): sqlite3. Test suite also tests some database
      functionalities based on SQLite 3. Same functionalities at runtime are
      only available if SQLite 3 is installed.
    + Update versioned B-D for curry-libs and curry-tools. Various fixes for
      the test runs shipped in those new package revisions.
    + Add to B-D and D (pakcs): lockfile-progs. Required by IOExts in curry-libs.
    + Typo fix in package SYNOPSIS. (Closes: #852950).
    + Add to S (pakcs): tk.
  * debian/rules:
    + Enable unit tests (via runtestverbose CHECK target).
    + Make sure that cleancurry is available via PAKCSHOME/bin at build time.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 03 Feb 2017 09:22:38 +0100

pakcs (1.14.1-2) unstable; urgency=medium

  * debian/control:
    + With curry-base 0.4.2-3 we don't need to enforce UTF-8 locale anymore. Now
      enforced in the curry-base code directly.
    + Version-bump B-Ds for curry-libs and curry-tools to 1.0.1. Required for
      new PAKCSINSTALLDIR handling.
    + Add D (pakcs): swi-prolog-nox (>= 7.2.3).
    + Update versioned B-D / D on curry-frontend. Make sure that 0.4.2-2~ or
      higher is installed, to assure having the UTF-8 fix on board.
    + Make package team-maintained (Debian Curry Maintainers, pkg-curry team on
      Alioth). Move myself to Uploaders: field. Add Michael Hanus (upstream) to
      Uploaders: field.
  * debian/rules:
    + Use cleanall target for upstream clean-up.
    + Additionally clean-up bin/, currytools/, debian/build/ and
      scripts/makesavedstate on clean target.
    + Set PAKCSINSTALLDIR and hand it over to upstream Makefile's build target.
    + Remove commented out DEB_MAKE_INSTALL_TARGET. Disable
      DEB_MAKE_CHECK_TARGET for now; it consumes a lot of time, memory and
      generates no output.
  * debian/patches:
    + Cherry-pick 0044-Addition-of-constant-for-package-install-dir-added.patch,
      0045-Installation-extended-to-PAKCSINSTALLDIR.patch,
      0048-Tools-update.patch, 0049-Tools-update.patch and
      0050-References-to-cymake-replaced-by-pakcs-frontend.patch from upstream.
      Adds PAKCSINSTALLDIR variable support to pakcs's main Makefile.
    + Drop unused patch file 0027-libs-updated.patch.
    + Add 0051-Prolog-module-imports-changed-to-avoid-SWI-errors.patch
      0052-pakcs-cymake-occurrence-removed.patch
      0053-SICStus-compilation-adapted-to-new-scheme-for-SWI-Pr.patch
      0054-Fix-for-loading-run-time-libraries-in-SICStus-back-e.patch
      (cherry-picked from upstream). Fix module loading in SWI Prolog.
    + Update 2001_run-cleancurry-from-inside-debian-subfolder.patch.
  * debian/pakcs.{install,links}:
    + Drop work-around for broken symlinks. Upstream now has relative symlinks.
  * debian/copyright:
    + Update copyright attributions.
  * debian/pakcs.lintian-overrides:
    + Fix typo in override comment.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 27 Jan 2017 14:23:32 +0100

pakcs (1.14.1-1) unstable; urgency=low

  * Initial release to Debian. (Closes: #840519).

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 23 Dec 2016 16:49:39 +0100
